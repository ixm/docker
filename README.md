# ImageX Docker
CI image for Bitbucket - https://hub.docker.com/r/imagexmedia/ci

Contains the following:
* MySQL Client
* Common PHP Extentions
* PhantomJs
* Chrome
* Phan
* Composer
* Drupal Console
* Global Drush 8 (For D6/7 projects)
* PHP Coding Standards
* PHP Copy/Paste detector
* PHP Debt calculation
* NodeJS / NPM
* Yarn
* Grunt
* Security Checker
* Terminus
